class CreateProcurements < ActiveRecord::Migration
  def change
    create_table :procurements do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :center, index: true, foreign_key: true
      t.string :purpose
      t.integer :status

      t.timestamps null: false
    end
  end
end
