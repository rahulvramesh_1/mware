class RemoveEmailFromSearch < ActiveRecord::Migration
  def change
    remove_column :searches, :email, :string
  end
end
