class AddDiscountToSuborders < ActiveRecord::Migration
  def change
    add_column :suborders, :discount, :decimal
  end
end
