class AddEmailToSuborder < ActiveRecord::Migration
  def change
    add_column :suborders, :email, :string
  end
end
