class CreateSuborders < ActiveRecord::Migration
  def change
    create_table :suborders do |t|
      t.date :start_date
      t.date :end_date
      t.decimal :total
      t.integer :day
      t.string :company
      t.string :product_name

      t.timestamps null: false
    end
  end
end
