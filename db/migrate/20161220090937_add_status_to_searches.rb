class AddStatusToSearches < ActiveRecord::Migration
  def change
    add_column :searches, :status, :boolean
  end
end
