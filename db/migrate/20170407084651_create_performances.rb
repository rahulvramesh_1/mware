class CreatePerformances < ActiveRecord::Migration
  def change
    create_table :performances do |t|
      t.string :type
      t.string :description
      t.integer :eta
      t.datetime :start
      t.datetime :end

      t.timestamps null: false
    end
  end
end
