class CreateOvertimes < ActiveRecord::Migration
  def change
    create_table :overtimes do |t|
      t.datetime :date
      t.integer :user_id
      t.string :note
      t.string :status
      t.integer :center_id

      t.timestamps null: false
    end
  end
end
