class RemoveEmailFromSuborder < ActiveRecord::Migration
  def change
    remove_column :suborders, :email, :string
  end
end
