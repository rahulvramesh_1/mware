class CreatePettyCashes < ActiveRecord::Migration
  def change
    create_table :petty_cashes do |t|
      t.date :transaction_date
      t.string :source
      t.string :recipient
      t.text :detail
      t.integer :p_category
      t.decimal :amount_in
      t.decimal :amount_out
      t.decimal :balance
      t.integer :status
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
