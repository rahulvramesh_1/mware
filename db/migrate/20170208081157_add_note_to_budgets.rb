class AddNoteToBudgets < ActiveRecord::Migration
  def change
    add_column :budgets, :note, :string
  end
end
