class AddAttachmentImageToProofs < ActiveRecord::Migration
  def self.up
    change_table :proofs do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :proofs, :image
  end
end
