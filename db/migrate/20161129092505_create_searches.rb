class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.string :company
      t.date :sdate
      t.date :edate

      t.timestamps null: false
    end
  end
end
