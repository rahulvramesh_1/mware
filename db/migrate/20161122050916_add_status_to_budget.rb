class AddStatusToBudget < ActiveRecord::Migration
  def change
    add_column :budgets, :status, :integer
    add_column :budgets, :action, :integer
  end
end
