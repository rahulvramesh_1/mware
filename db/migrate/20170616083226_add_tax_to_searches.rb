class AddTaxToSearches < ActiveRecord::Migration
  def change
    add_column :searches, :tax, :boolean
  end
end
