# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170727074037) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "budgets", force: :cascade do |t|
    t.date     "date"
    t.string   "id_request"
    t.integer  "user_id"
    t.integer  "department_role_id"
    t.text     "details"
    t.integer  "quantity"
    t.string   "vendor"
    t.decimal  "amount"
    t.decimal  "cash_in"
    t.decimal  "cash_out"
    t.decimal  "balance"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "status"
    t.integer  "action"
    t.string   "note"
  end

  create_table "centers", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "tag"
    t.string   "tax"
    t.string   "receivable"
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "cc"
    t.string   "center"
  end

  create_table "department_roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "department_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "departments", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "items", force: :cascade do |t|
    t.string   "product"
    t.integer  "quantity"
    t.decimal  "price"
    t.decimal  "total"
    t.integer  "budget_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "type_product"
    t.integer  "status"
    t.string   "vendor"
  end

  create_table "overtimes", force: :cascade do |t|
    t.datetime "date"
    t.integer  "user_id"
    t.string   "note"
    t.string   "status"
    t.integer  "center_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "end"
    t.decimal  "amount"
  end

  create_table "performances", force: :cascade do |t|
    t.string   "type"
    t.string   "description"
    t.integer  "eta"
    t.datetime "start"
    t.datetime "end"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "petty_cashes", force: :cascade do |t|
    t.date     "transaction_date"
    t.string   "source"
    t.string   "recipient"
    t.text     "detail"
    t.integer  "p_category"
    t.decimal  "amount_in"
    t.decimal  "amount_out"
    t.decimal  "balance"
    t.integer  "status"
    t.integer  "user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "procurements", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "center_id"
    t.string   "purpose"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "procurements", ["center_id"], name: "index_procurements_on_center_id", using: :btree
  add_index "procurements", ["user_id"], name: "index_procurements_on_user_id", using: :btree

  create_table "product_lists", force: :cascade do |t|
    t.string   "name"
    t.decimal  "price"
    t.string   "link"
    t.boolean  "approve"
    t.integer  "procurement_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "product_lists", ["procurement_id"], name: "index_product_lists_on_procurement_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.string   "jurnal_name"
    t.integer  "center_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "unit"
  end

  add_index "products", ["center_id"], name: "index_products_on_center_id", using: :btree

  create_table "proofs", force: :cascade do |t|
    t.integer  "budget_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "searches", force: :cascade do |t|
    t.string   "company"
    t.date     "sdate"
    t.date     "edate"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.boolean  "status"
    t.string   "inv"
    t.string   "note"
    t.boolean  "paid"
    t.boolean  "tax"
    t.datetime "invoice_date"
  end

  create_table "suborders", force: :cascade do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.decimal  "total"
    t.integer  "day"
    t.string   "company"
    t.string   "product_name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "email"
    t.string   "center"
    t.decimal  "discount"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "department_id"
    t.string   "fullname"
    t.integer  "center_id"
    t.decimal  "ot_fee"
    t.integer  "head"
    t.integer  "clvl"
    t.integer  "manager"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "procurements", "centers"
  add_foreign_key "procurements", "users"
  add_foreign_key "product_lists", "procurements"
  add_foreign_key "products", "centers"
end
