json.extract! suborder, :id, :start_date, :end_date, :total, :day, :company, :product_name, :created_at, :updated_at
json.url suborder_url(suborder, format: :json)