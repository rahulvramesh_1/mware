class ApplicationMailer < ActionMailer::Base
  default from: "admin@evhive.co"
  layout 'mailer'
end
