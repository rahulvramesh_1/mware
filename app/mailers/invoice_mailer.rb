class InvoiceMailer < ApplicationMailer

  def gen_inv(search,gg,email,ct,dv,tv)
    @search = search
    @gg = gg
    @subtotal_value = ct
    @discount_value = dv
    @total_value = tv
    @email = email

    get_center = @search.search_suborders.pluck("center").first

    get_center_id = Center.where('tag = ?', get_center).pluck("id").first

    # get_facility = User.where('manager = ? and department_id = ? and center_id = ?',1, 3, get_center_id).pluck("email").first
    get_facility = User.where('manager = ? and department_id = ? and center_id = ?',1, 3, get_center_id).collect(&:email).join("; ")
    
    mail(:to => @email, :cc => ["finance-accounting@evhive.co",get_facility,"niken@evhive.co"], :subject => "Ev Hive Invoice") do |format|
      format.text { render text: "This is your Invoice. Once you have clear your payment, please mail us at finance-accounting@evhive.co\n\n Thank You \n EV HIVE" }
      format.pdf do
        attachments['invoice.pdf'] = WickedPdf.new.pdf_from_string(
          render_to_string(:pdf => 'invoice',:template => 'searches/show.pdf.erb',:locals => {:search => @search}, :page_height => '5.5in',:page_width =>'8.5in',:encoding => 'UTF-8')
        )
      end
    end

  end

  def gen_payment(search,gg,email,ct,dv,tv)
    @search = search
    @gg = gg
    @subtotal_value = ct
    @discount_value = dv
    @total_value = tv
    @email = email

    get_center = @search.search_suborders.pluck("center").first

    get_center_id = Center.where('tag = ?', get_center).pluck("id").first

    # get_facility = User.where('manager = ? and department_id = ? and center_id = ?',1, 3, get_center_id).pluck("email").first
    get_facility = User.where('manager = ? and department_id = ? and center_id = ?',1, 3, get_center_id).collect(&:email).join("; ")

      mail(:to => @email, :cc => ["finance-accounting@evhive.co",get_facility,"niken@evhive.co"], :subject => "Ev Hive Payment Receipt") do |format|
        format.text { render text: "This is your payment receipt.\nfinance@evhive.co\n\nThank You \nEV Hive" }
        format.pdf do
          attachments['payment_receipt.pdf'] = WickedPdf.new.pdf_from_string(
            render_to_string(:pdf => 'payment_receipt',:template => 'payment_receipt/show.pdf.erb',:locals => {:search => @search}, :page_height => '5.5in',:page_width =>'8.5in',:encoding => 'UTF-8')
          )
        end
      end
  end

end
