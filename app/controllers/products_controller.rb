class ProductsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  # GET /products
  # GET /products.json
  def index
    @products = Product.where('center_id = ?',current_user.center_id)
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        create_jurnal_product
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def create_jurnal_product
    require 'uri'
    require 'net/http'

    if Rails.env.production?
      url = URI("https://api.jurnal.id/core/api/v1/products?apikey=906dbd5f8453c9a323b6f9b056690c96")
    else
      url = URI("https://sandbox-api.jurnal.id/core/api/v1/products?apikey=c51a5dfab205e5030e4d7707d12dc42e")
    end

    http = Net::HTTP.new(url.host, url.port)
    if Rails.env.production?
    http.use_ssl = true
    else
    http.use_ssl = true
    end
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    an = "["+@product.center.name+"] Revenue"

    @payload = {
        "product": {
        "name": @product.jurnal_name,
        "sell_account_name": an,
        "is_sold": true,
        "product_category_names": [an],
        "unit_name": @product.unit
                          } }.to_json

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'application/json'
    request.body = @payload

    response = http.request(request)
    puts response.read_body
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:name, :jurnal_name, :center_id, :unit)
    end
end
