class SearchesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_search, only: [:edit,:update]
  include SearchesHelper
  def index
    @searches = Search.all.order(id: :desc)
  end

  def new
    @search = Search.new
    @companies = Suborder.where('center like ?', '%'+current_user.center.name+'%').uniq.pluck(:company)
  end

  def create
    @search = Search.create(search_params)
    @search.edate = @search.sdate
    respond_to do |format|
      if @search.save
        format.html { redirect_to @search, notice: 'Company was successfully created.' }
        format.json { render :show, status: :created, location: @search }
      else
        format.html { render :new }
        format.json { render json: @search.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
    @search.edate = @search.sdate
    respond_to do |format|
      if @search.update(search_params)
        format.html { redirect_to @search, notice: 'Petty cash was successfully updated.' }
        format.json { render :show, status: :ok, location: @search }
      else
        format.html { render :edit }
        format.json { render json: @search.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @search = Search.find(params[:id])
    @search.destroy
    respond_to do |format|
      format.html { redirect_to searches_path, notice: 'inv was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def show
    @search = Search.find(params[:id])
    trans_builder
    trans_detail
    gen_email
    respond_to do |format|
      format.html do
        render template: "searches/show.html.erb"
      end
       format.pdf do
         render pdf: "invoice",
                template: "searches/show.pdf.erb",
                locals: {:search => @search},
                encoding: 'UTF-8'
       end
    end
  end

  def send_jurnal
    x = Search.order(:invoice_date).pluck(:inv).compact.last
    y = x.to_s
    # generating invoice number
    lnum = gen_inv(y)
    @search = Search.find(params[:id])
    # get email if user not found
    gen_email
    # get tag
    @tag = Suborder.where(company: @search.company).pluck(:center).compact.first
    #item counter
    desc = "Membership " + Date.today.strftime("%B") + " " + Date.today.strftime("%Y") + " (Prorated)"
    tdate = Date.today
    ddate = Date.today + 5.days
    # get center
    @center = @search.search_suborders.pluck(:center).compact.first
    # get company tax tag
    tax = Center.where('tag = ?', @center).pluck("tax").first

    trans_builder
    # api url
    if Rails.env.production?
      url = URI("https://api.jurnal.id/core/api/v1/sales_invoices")
    else
      url = URI("https://sandbox-api.jurnal.id/core/api/v1/sales_invoices?apikey=c51a5dfab205e5030e4d7707d12dc42e")
    end
    # initiating transmision
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    # initiating payload
    if @search.tax == true
      @payload = {
          "sales_invoice": {
          "transaction_date": tdate,
          "due_date": ddate,
          "transaction_lines_attributes": @gg,
          "witholding_value": 10,
          "witholding_type":"percent",
          "witholding_account_name": tax,
          "person_name": @search.company,
          "email": @email,
          "message": "Thank you for choosing EV Hive!",
          "tags":[@center],
          "memo": @search.company,
          "transaction_no": lnum
                            } }.to_json
    else
      @payload = {
          "sales_invoice": {
          "transaction_date": tdate,
          "due_date": ddate,
          "transaction_lines_attributes": @gg,
          "person_name": @search.company,
          "email": @email,
          "message": "Thank you for choosing EV Hive!",
          "tags":[@center],
          "memo": @search.company,
          "transaction_no": lnum
                            } }.to_json
    end
    # sending post request
    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'application/json'
    request["apikey"] = '906dbd5f8453c9a323b6f9b056690c96'
    request.body = @payload
    response = http.request(request)
    puts response.read_body

    if response.message == "Created"
      @search.update_attributes(status: true)
      @search.update_attributes(invoice_date: Time.now)
      @search.update_attributes(inv: lnum)
      redirect_to @search, notice: response.message
    else
      create_customer
    end

    # respond_to do |format|
    #   format.html {  }
    # end

  end

  def send_report

    @search = Search.find(params[:id])
    trans_builder
    trans_detail
    gen_email
    InvoiceMailer.gen_inv(@search,@gg,@email,@subtotal_value,@discount_value,@total_value).deliver_now

    respond_to do |format|
      format.html { redirect_to @search, notice: 'send' }
    end
  end

  def create_customer
    require 'uri'
    require 'net/http'

    if Rails.env.production?
      url = URI("https://api.jurnal.id/core/api/v1/customers?apikey=906dbd5f8453c9a323b6f9b056690c96")
    else
      url = URI("https://sandbox-api.jurnal.id/core/api/v1/customers?apikey=c51a5dfab205e5030e4d7707d12dc42e")
    end

    http = Net::HTTP.new(url.host, url.port)
    if Rails.env.production?
    http.use_ssl = true
    else
    http.use_ssl = true
    end
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    ap = "Accounts Payable"

    ar = Center.where('tag = ?', @center).pluck("receivable").first

    @payload = {
        "customer": {
        "display_name": @search.company,
        "email": @email,
        "default_ar_account_name": ar,
        "default_ap_account_name": ap
                          } }.to_json

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'application/json'
    request.body = @payload

    response = http.request(request)
    puts response.read_body

    send_jurnal
  end

  private

  def set_search
    @search = Search.find(params[:id])
  end


  def gen_inv(y)
    cday = Date.today
    #get real time month
    cmon = Date.today.strftime('%m').to_i
    #get last transformed month to integer
    pmon = y.split('/')[-2]
    mmon = to_numb(pmon)
    #get current number for increment
    cnum = y.split('/')[-1].to_i
    sep = "/"
    #compare last inserted month with real time month
    if mmon == cmon
      #add last id + 1
      myno =  cnum + 1
      #if genereated id < 10
      if myno < 10
        lno = "0" + myno.to_s
      else
        lno = myno.to_s
      end
      #generated invoice id
      inv_no = "I/" + Date.today.year.to_s + sep + to_roman(cmon) + sep + lno
      # inv_no = lno + sep + to_roman(cmon) + sep + Date.today.year.to_s
      return inv_no
    #if last month not equals than
    elsif mmon != cmon
      lno = "01"
      inv_no = "I/" + Date.today.year.to_s + sep + to_roman(cmon) + sep + lno
      # inv_no = lno + sep + to_roman(cmon) + sep + Date.today.year.to_s
      return inv_no
    end
  end

  def to_roman(a)

    if a == 1
      return "I"
    elsif a == 2
      return "II"
    elsif a == 3
      return "III"
    elsif a == 4
      return "IV"
    elsif a == 5
      return "V"
    elsif a == 6
      return "VI"
    elsif a == 7
      return "VII"
    elsif a == 8
      return "VIII"
    elsif a == 9
      return "IX"
    elsif a == 10
      return "X"
    elsif a == 11
      return "XI"
    elsif a == 12
      return "XII"
    end
  end

  def to_numb(a)

    if a == "I"
      return 1
    elsif a == "II"
      return 2
    elsif a == "III"
      return 3
    elsif a == "IV"
      return 4
    elsif a == "V"
      return 5
    elsif a == "VI"
      return 6
    elsif a == "VII"
      return 7
    elsif a == "VIII"
      return 8
    elsif a == "IX"
      return 9
    elsif a == "X"
      return 10
    elsif a == "XI"
      return 11
    elsif a == "XII"
      return 12
    end

  end

  def search_params
    params.require(:search).permit(:company, :sdate, :edate, :email, :note,:tax, :invoice_date, suborders_atributes:[])
  end

end
