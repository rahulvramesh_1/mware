class Budgets::RequestController < BudgetsController
  def index
    if current_user.department_id == 1 && current_user.clvl == 1
      @budgets =  Budget.where('status = 3')
    elsif current_user.department_id == 1
      @budgets = Budget.where('status = 1')
    elsif current_user.department_id != 1 and current_user.head == 1
      @budgets =  Budget.joins(:user).where('budgets.status = 1 and users.department_id = ?', current_user.department_id)
    else
      @budgets = Budget.where('status = 1 and user_id = ?', current_user)
    end
  end
end
