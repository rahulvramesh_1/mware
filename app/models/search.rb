class Search < ActiveRecord::Base
  has_many :suborders

  def search_suborders
    suborders = Suborder.all
    suborders = suborders.where(["company LIKE ?", company]) if company.present?
    suborders = suborders.where(["start_date BETWEEN ? AND ?", sdate, edate ]) if sdate.present?

    return suborders
  end

  def transaction_list
    # standard
    x = search_suborders.select('sum(total) as rate, total, product_name, count(product_name) as quantity, start_date, end_date').group('product_name, total, start_date, end_date').map{|f| {:rate => f.total, :product_name => f.product_name, :quantity => f.quantity ,:discount => 0 } }
    # discounted company
    y = search_suborders.select('sum(total) as rate, total, product_name, count(product_name) as quantity, start_date, end_date, discount').group('product_name, total, start_date, end_date,discount').map{|f| {:rate => f.total, :product_name => f.product_name, :quantity => f.quantity ,:discount => f.discount_rate } }
    if company == "PT Kreasi Asia Pasifik"
      list = x
      tax_value = list.map{|s| s[:rate]}.reduce(0,:+).to_i * 10/1000
      tax_item = [{:rate => tax_value , :product_name => "[The Maja] Tax Service Fee", :quantity => 1, :discount => 0 }]
      list = list + tax_item
    # elsif company == "PT Ninety Nine Dotco" || company == "PT Kuliner Digital Sejahtera" || company == "Sirclo"
      # list = y
    else
      list = y
    end

    return list
  end

  def transaction_subtotal
    transaction_list.map{|s| s[:quantity] * s[:rate] }.reduce(0,:+).to_i
  end

  def transaction_discount
    transaction_list.map{|s| s[:quantity] * s[:rate] *  s[:discount] / 100  }.reduce(0,:+).to_i
  end

  def transaction_tax
    (transaction_subtotal - transaction_discount) * 10 /100
  end

  def transaction_total
    if tax == true
      transaction_subtotal - transaction_discount - transaction_tax
    else
      transaction_subtotal - transaction_discount
    end
  end
end
