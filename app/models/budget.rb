class Budget < ActiveRecord::Base
  belongs_to :user
  has_many :items, :dependent => :destroy
  has_many :proofs, :dependent => :destroy
  accepts_nested_attributes_for :items,  :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :proofs,  :reject_if => :all_blank, :allow_destroy => true

  # private
    def subtotal
     items.collect {|oi|(oi.quantity * oi.price) }.sum
   end
end
