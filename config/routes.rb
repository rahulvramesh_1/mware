Rails.application.routes.draw do


  root :to => "budgets#index"

  namespace :budgets do
    resources :request do
      scope module: :request do
      end
    end
    resources :spend do
      scope module: :spend do
        resources :budgets
      end
    end
    resources :revise do
      scope module: :revise do
        resources :budgets , :only => [:index,:show, :create, :update]
      end
    end
    resources :approve do
      scope module: :approve do
        resources :budgets
      end
    end
  end

  namespace :overtimes do
    resources :request do
      scope module: :request do
        resources :overtimes
      end
    end
    resources :approve do
      scope module: :approve do
        resources :overtimes
      end
    end
    resources :reject do
      scope module: :reject do
        resources :overtimes
      end
    end
  end

  resources :suborders
  get "payment_receipt/send_payment/:id" => "payment_receipt#send_payment"
  get "searches/send_jurnal/:id" => "searches#send_jurnal"
  get "budgets/submit_jurnal/:id" => "budgets#submit_jurnal"
  get "budgets/fund_transfer/:id" => "budgets#fund_transfer"
  get "searches/send_report/:id" => "searches#send_report"
  get 'invoices/search'
  resources :searches
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :budgets, :only => [:index,:show, :create, :update]
      resources :suborders, :only => [:index,:show, :create, :update]
    end
      resources :suborders, :only => [:index,:show, :create, :update]
  end

  get 'spends/new'
  get 'requests/new'
  devise_for :users, :skip => [:registrations]
    as :user do
      get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
      put '/users(.:format)' => 'devise/registrations#update', as: 'user_registration'
      patch '/users(.:format)' => 'devise/registrations#update'
    end

  resources :budgets do
    collection do
      put :send_jurnal
    end
  end
  resources :procurements
  resources :products
  resources :performances
  resources :payment_receipt
  resources :statistic
  resources :companies
  resources :users
  resources :overtimes
  resources :petty_cashes
  resources :department_roles
  resources :departments
  resources :centers

end
